## WPezClasses: Images Size Register

__WordPress add_image_size() done The ezWay.__
   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\ImageSizesRegister\ClassImageSizeRegister as ISR;
use WPezSuite\WPezClasses\ImageSizesRegister\ClassHooks as Hooks;

$arr_imgs = [
    'size1' => [
        'name' => 'sqr_md',
        'width' => '500',
        'height' => '500'
        'thumbnail' => true
        ],
    'size2' => [
        'name' => 'sqr_lg',
        'width' => '650',
        'height' => '650'
        ]
    ];
    
$arr_img = [
        'name' => 'sqr_xl',
        'width' => '800',
        'height' => '800'
        'crop' => true
    ];
    
$new_isr = new ISR();
// load via array
$new_isr->loadImages($arr_imgs);
// or push one at a time
$new_isr->pushImage($arr_img);
    
$new_hooks = new Hooks($new_isr);
$new_hooks->register();

```

### FAQ

__1) Why?__

Because it's wiser and ez'ier to configure an array than it is to hard-code a list of add_image_size() functions. Arrays can be pulled from a library. They can be manipulated. They can be fed from a config file into some pre-tested boilerplate code. Etc.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 

### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/ImageSizeNamesChoose

- https://developer.wordpress.org/reference/functions/add_image_size/

- https://codex.wordpress.org/Function_Reference/set_post_thumbnail_size

### TODO

n/a

### CHANGE LOG

- v0.0.4 - Monday 22 April 2109
    - UPDATED: interface file / name

- v0.0.3 - Thursday 18 April 2019
    - ADDED: support for set_post_thumbnail_size();

- v0.0.2 - Wednesday 17 April 2019
    - Changed: namespace and class name. Also updated README with link to ImageSizeNamesChoose

- v0.0.1 - Wednesday 17 April 2019
    - Hey! Ho!! Let's go!!! Yet another "finally doing a proper repo of this tool." Please pardon the delay