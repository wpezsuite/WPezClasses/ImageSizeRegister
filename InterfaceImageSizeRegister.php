<?php

namespace WPezSuite\WPezClasses\ImageSizeRegister;

interface InterfaceImageSizeRegister {

	public function registerImageSizes();

}